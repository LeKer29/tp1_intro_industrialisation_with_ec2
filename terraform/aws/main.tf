terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.12.0"
    }
  }
}

provider "aws" {
  # Configuration options
  region = var.region
}

resource "aws_key_pair" "deployer" {
  key_name   = "key-deployer"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDAn2tlzvGYRGtvwV7jSRlDfESO+rHvEUL3wnYm9NJ6GTWl5xjWsEMQ92YNyjMeKq7T6rkZyJxu5UJWsAfeFF8jRLTC+p4xrwW0nI8jhkmZuTzs/RlQ6oUCPqdCqJcnCKr3BCVzmz1chpqh21EGVOP6HlGwwGbIftly/QU3cllEz6IgA74wCv2kEYtnYlxrqSZbQ3l5EexXOp22FVyRj9VLevBnC3OPw5iI5NpmjSJ18D8BL//l/S2mYrfto+UeQHOa2RlVm8iUjDCsh6q8tCIYpHev17k/F8XNS1odZzwj65qn+hK8devDISqQIwQ+dc+O3/KwRGKOC8z3BJnfIkhGOZkfyprTQXoylblTtzWOeMtBOiKq9ajHHku9puTDozEA4LirQaQegtvVz0A++p/U+mIScla4/Kg5PtRq5vqETmg9PJlmdiumMkjfMH8vx8XOXlWpb/ilHane04RepY3CB2sjZqoGNQeKMW8qdSBlv1ok5Ow10WIsOxs/86byLN8= cdo@ubuntu"
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_default_vpc.default.id

  ingress {
    protocol  = "TCP"
    from_port = 80
    to_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol  = "TCP"
    from_port = 22
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "web" {
  ami           = var.ami_id
  instance_type = var.instance_type
  key_name = aws_key_pair.deployer.key_name

  tags = {
    Name = "HelloWorld"
  }
}